# what is javascript?
 - JavaScript is a scripting language commonly used in web development (Both front-end and back-end). It was originally developed by Netscape as a means to add dynamic and interactive elements to websites. ... 
 **Example**:- For example, a JavaScript function may check a web form before it is submitted to make sure all the required fields have been filled out.
### - In every update Javascript comes with the new features, thats why it is developers choice. 
## HISTORY OF JAVASCRIPT.
- **Brendan Eich** created
JavaScript in 1995 while he was at Netscape Cimmunication.
_**(Netscape Communications Corporation: It was an American independent 
computer services company with headquarters in Mountain View, 
California and then Dulles, Virginia. )**_
-  Netscape and Eich designed JavaScript as a scripting language for use with the company's flagship web browser, Netscape Navigator. 
- Initially known as LiveScript, Netscape changed the name to JavaScript so they could position it as a companion for the Java language, a product of their partner, Sun Microsystems.
-  Apart from some superficial syntactic similarities, though, JavaScript is in no way related to the Java programming language.
- After its release, more and more browsers started adding JavaScript support. Still, for much of its history JavaScript was not regarded as a serious programming language. Its earliest releases suffered from notable performance and security issues, but developers had no alternatives. If they wanted to run programs in the browser, they had to use JavaScript.
- In 2008, the creation of Google's open-source Chrome V8, a high-performance JavaScript engine, provided a crucial turning point for JavaScript. 
_**(The subsequent proliferation of fast JavaScript engines made it possible for developers to build sophisticated browser-based applications with performance that competed with desktop and mobile applications.)**_
- Soon after, Ryan Dahl released an open-source, cross-platform environment called Node.js. It provided a way to run JavaScript code from outside a browser.
## ADVANTAGES OF JAVASCRIPT:
- Speed. Client-side JavaScript is very fast because it can be run immediately within the client-side browser.
- Simplicity. JavaScript is relatively simple to learn and implement.
Popularity.
- Popularity. JavaScript is used everywhere on the web.
Interoperability. JavaScript plays nicely with other languages and can be used in a huge variety of applications.
- Gives the ability to create rich interfaces.
- Variety of application: like java script used in many areas.
## DISADVANTAGE OF JAVASCRIPT:
- Client-Side Security. Because the code executes on the users’ computer, in some cases it can be exploited for malicious purposes. This is one reason some people choose to disable Javascript.
- Browser Support. JavaScript is sometimes interpreted differently by different browsers. This makes it somewhat difficult to write cross-browser code.
- Performance. Javascript is quite slow as compare to other programming languages.
## APPLICATION OF JAVASCRIPT (USECASES):
### 1) Adding interactive behavior to web pages
- Show or hide more information with the click of a button
- Change the color of a button when the mouse hovers over it
- Slide through a carousel of images on the homepage
- Zooming in or zooming out on an image
- Displaying a timer or count-down on a website
- Playing audio and video in a web page
- Displaying animations
- Using a drop-down hamburger menu
### 2) Creating web and mobile apps:
- Developers can use various JavaScript frameworks for developing and building web and mobile apps. JavaScript frameworks are collections of JavaScript code libraries that provide developers with pre-written code to use for routine programming features and tasks—literally a framework to build websites or web applications around. 
### 3) Building web servers and developing server applications:
- Beyond websites and apps, developers can also use JavaScript to build simple web servers and develop the back-end infrastructure using Node.js. 
### 4) Game development:
_**Q. IS JAVASCRIPT GOOD FOR GAME DEVELOPMENT?**_ 
- Yes! JavaScript is a great language for game development, depending on the type of game you want to create. JavaScript is best for web-based and mobile games.
- Using platforms and tools can help create both 2D and 3D games that can run directly in your browser. Aside from only web-based games, JavaScript has been increasing in popularity in mobile game development. 
- On the contrary, if you’re looking to create the next big AAA game, like Call of Duty or FIFA, using JavaScript, you may find it challenging. 
scripting language.
- It mainly handles the behavior of user actions
- JavaScript is a very powerful tool that can do many things for a website. For one, it powers the site’s general interactivity. JavaScript makes it possible to 
### Popular games in JavaScript:
- **Tower Building** is a great way to get started with JavaScript games. The game allows players to stack blocks to create a very tall tower.
- **Bejeweled** was created as an in-browser game in the early 2000s. It’s similar to Candy Crush where you have to match three jewels in a row to score points. 
- **Polycraft** is a **3D** game that is playable in your browser. Polycraft is full of adventure, exploration, base-building, gathering, crafting, and even fighting. It’s an excellent example of how you can move past 2D games with Javascript
- _**What Are the Best JavaScript Game Engines?**_
#### - PixiJS
#### - BabylonJS.

## HOW TO WRITE JAVASCRIPT CODE:
1. We can write javascript code in between the HTML body, with "script" tag.
2. Or we can make .js extension file and later we can insert it's location in HTML codes.
## USE OF JAVASCRIPT IN FRONT-END WEB DEVELOPMENT:
- JavaScript was originally designed as a client-side build rich UI components such as image sliders, pop-ups, site navigation mega menus, form validations, tabs, accordions, and much more.
- JavaScript allows webpages to respond to user activity and dynamically update themselves, and all without requiring a page reload to change its appearance. 
- A client-side script is a programming language that performs its tasks entirely on the client's machine and does not need to interact with the server to function.
- if you have a Web page loaded on your computer and your Internet service provider goes down, you are still able to interact with the Web pages already loaded on your browser.
- JavaScript is able to communicate with the server in the background without interrupting the user interaction taking place in the foreground. 
### React.js is best platform for front-end development.
## USE OF JAVASCRIPT IN BACK-END DEVELOPMENT:
- **There are lot of framework and application to use javascript in back-end development.**
### 1. NodeJS:
- Node.js is a platform built on Chrome's JavaScript runtime for easily building fast, scalable network applications. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices.
**For detail information about nodeJS you can go through this-** https://nodejs.dev/learn
### 2. OPA:
- Opa is an advanced application framework for JavaScript. All aspects are directly written in Opa: Frontend code, backend code, database queries and configuration. And everything is strongly statically typed.
**For detail information click here-** https://dzone.com/articles/introduction-opa
### 3. CommonJS:
- The CommonJS API will fill that gap by defining APIs that handle many common application needs, ultimately providing a standard library as rich as those of Python, Ruby and Java. 
**For detail information click here-** https://www.oreilly.com/library/view/learning-javascript-design/9781449334840/ch11s03.html
#### NOTE: 
Each browser has its own JavaScript engine which is used to support the JavaScript scripts in order for them to work properly. The basic job of a javascript engine is to take the javascript code, then convert it into a fast, optimized code that can be interpreted by a browser.
- for example: Chrome:V8 is a javascript engine for google chrome browser.
## HOW TO RUN JAVASCRIPT CODE IN DIFFERENT PLATFORM.
### IN GOOGLE CHROME:
- You have to open chrome and type **ctrl+shift+j** and it will open javascript console where you can write code and test.
### IN SERVER :
- You can use **NodeJS**_(backend)_ and **reactJS**(_frontend_) to run your code in server.It is a runtime which allows you to execute JavaScript outside the browser.
## WHY JAVASCRIPT IS SO SUCCESSFUL
- **In short, JavaScript became a hit because it turned web browsers into application platforms. Here's how: JavaScript can be used in both the front-end and back-end of web development. ... Chrome, the most used Internet browser in 2017, has been so successful in part because of its ability to process JavaScript quickly.** 
- **JavaScript is a client-side programming language which helps web developer to do Web Application Development and make dynamic and interactive web pages by implementing custom client-side scripts.**
## Biggest website built in javascript.
1. **Google(search engine) - it uses javascript for both front end and back end.**
2. **Youtube**  
3. **Facebook**
4. **Wikipedia**
5. **Yahoo!**
6. **Amazon**
7. **eBay**
## Q. CAN WE FIND THE JAVASCRIPT CODE?
- Yes we as a user can find javascript code for any website.Just right click and go to the **Incpect** there you will find _**HTML, CSS and Javascript code**_ and you can also edit it for limited time.
## SYLLABUL OF JAVASCRIPT:
## Section 1 (CORE/BASIC)- 
### 1.Fundamentals of javascript code. 
- Variables
- The importance of quotes.
- Numbers vs strings
- Concatnation(operation of joining char strings)
### 2.Datatype and constant.
- Variable
- Operators
### 3.Function.
- Defining functions
- Calling functions
- Defining parameters & passing arguments

### 4.If Statements:
- Testing code in javascript console.
- Getting & setting properties
- Using if statements
### 5.Strings:
- Strings are enormously important, too. A string is a series of characters, like 'Hello!'
### 6.Boolean logic:
- Boolean logic is used by all programming languages. It defines the user of AND (&&), OR (||) and NOT (!).
### 7.The THIS keyword:
- **This** is very powerful keyword.The JavaScript this keyword refers to the object it belongs to.
### 8.Loops in javascript:
1. For loop
2. While loop
3. Do-while loop
4. For in loop.(The JavaScript for in loop is used to iterate the properties of an object)
### 9.Array and objects.
- In JavaScript, an array is an ordered list of values. Each value is called an element specified by an index.
- Both objects and arrays are considered “special” in JavaScript. Objects represent a special data type that is mutable and can be used to store a collection of data
## Section 2(Advance Javascript):
### 1. ADVANCE FUNCTION
  - Recursion
  - Closure
  - The 'new function'
  - Arrow function
  - Rest parameter and Spread Operator
  - Global function
  - SetTimeout & SetInterval
  - Function binding
### 2. JAVASCRIPT NAMESPACE
  - JavaScript does not support namespaces. But the namespaces are important as they help reduce the number of identifiers for variables, objects, and functions that are added to the global scope in your application.
  - **Why do we need namespace?**
  - Namespaces are used to organize code into logical groups and to **prevent name collisions** that can occur especially when your code base includes multiple libraries.
### 3. PROTOTYPES
  - **In JavaScript, objects have a special hidden property known as Prototype which depicts either null or references another object.The following types are:-**
  - Prototype inheritance
  - prototype method, object.
### 4. ERROR HANDLING
  - **Try catch**:- it is use to catch errros
### 5. MODULES IN JAVASCRIPT
  - A module is a self-contained piece of code that groups semantically-related variables and functions. Modules are not built-in constructs in JavaScript. But the JavaScript Module Pattern provides a way to create modules that have well-defined interfaces that are exposed to clients of the module.
### 6. CHANGING JAVASCRIPT METHODS
  - JavaScript allows you to invoke multiple methods on an object in a single expression. To invoke multiple methods, we have chaining. Chaining is a process of stringing the method calls together with dots between them.
  - **Syntax:** object.method1().method2().method3();
## Section 3(DOM "Document object model"):
**Defination**- The Document Object Model (DOM) is a programming interface for HTML and XML(Extensible markup language) documents.
- **With the object model, JavaScript gets all the power it needs to create dynamic HTML:**
  - Javascript gets all the power to change the HTML elements in page.
  - Javascript can change the attributes of HTML page
  - Javascript can react all the existing HTML events

**How does Javascript interact with DOM?**
- JavaScript makes the HTML page active and dynamic via the DOM. ... It does this by communicating with the browser using the properties, methods, and events in the interface called the Document Object Model, or DOM.
## Section 4(BOM " Browser Object Model"):
### 1.Popups.
- Popups are windows opened by JavaScript. The advantage is that you can specify the appearance of popups in great detail.
### 2.Cookies.
- Cookies store information about your users or their preferences. This information is stored in your users' browsers and is available when that particular user visits your site again.

## MULTIPLE KEYWORDS IN JAVASCRIPT:-
- abstract,arguments,bolean,break, byte, case, catch, char, const, continue, debugger, default, delete, do, double,else, float, final, false, finally, eval, for, function, goto, if, implements, in , instanceof, int, interface, let, long, native, new, null, package, private,  protected, public return, short, static, switch, synchronized, this, throw, throws, transient, true, try, typeof,var,void, volatile, while, with, yield.

## SYNCHRONOUS AND ASYNCHRONOUS CONCEPT IN JAVASCRIPT:
### Synchronous-
- As the name suggests synchronous means to be in a sequence, i.e. every statement of the code gets executed one by one.
- So, basically a statement has to wait for the earlier statement to get executed.
### Asynchronous-
- Asynchronous code allows the program to be executed immediately where the synchronous code will block further execution of the remaining code until it finishes the current one.
- This may not look like a big problem but when you see it in a bigger picture you realize that it may lead to delaying the User Interface.

**Q. Why do we use asynchronous javascript?**
- JavaScript is a single-threaded programming language which means only one thing can happen at a time. ... Using asynchronous JavaScript (such as callbacks, promises, and async/await), you can perform long network requests without blocking the main thread.
## BLOCKING AND NON-BLOCKING IN NODEJS:
### Blocking-
- When javascript execution in Node.js process (each program is a process) has to wait until a non-javascript operation completes is called blocking.
- This happens because the event loop is unable to continue running JavaScript while a blocking operation is occurring.
### Non-blocking-
- A non-blocking call in JavaScript provides a callback function that is to be called when the operation is complete. 
-  Non-blocking responds immediately if the data is available and if not that simply returns an error.
- Non-Javascript execution refers to mainly I/O operations. So, in the nutshell, I/O operations are blocking.

**NOTE**- **Blocking methods execute synchronously and non-blocking methods execute asynchronously.**
## JAVASCRIPT PROGRAMS:
**For online javascript go through this link-** https://replit.com/@RashidIqbal1/Hellojs#index.js




