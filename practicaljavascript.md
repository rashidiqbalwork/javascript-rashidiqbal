## HOW TO TEST JAVASCRIPT CODE IN BROWSER?
- **There are several tools and techniques available using which one can test JavaScript in a browser. Each tool comes with a set of unique features that contribute to different outcomes.**
### JSFiddle- 
- **Whether you are working with JavaScript or frameworks like React and Vue, JSFiddle is the tool for you. It is an online tool to write and test JavaScript code in a browser. It was launched as "Mooshell" in 2009**
- **The interface is extremely simple and you just need to type some JavaScript or even add some HTML and CSS to it and instantly see the results.**
### Cross Browser Testing Tools-
-  **In case you want to test your code for cross-browser compatibility you need to use online cross-browser testing tools like LambdaTest. It enables live interactive browser testing, automated screenshot testing, responsive layout testing, and smart visual UI testing.**
### Karma + Jasmine + Google Chrome-
- **Karma is a tool that lets you test JavaScript code in a browser for a lot of testing purposes. However, it does not test the code itself.**
- **It executes the code but relies on third-party libraries like Jasmine and Mocha for testing. Apart from that, it requires a real browser. Thus Google Chrome must be installed on your local machine for this method of JavaScript to work.**
### Codepen-
- **CodePen is one of the best online tools to test your HTML, CSS and JavaScript code online. This community of developers has a lot to teach!**
- **It is one of the best platforms to build and deploy a website, show the world your work and build test cases.**

**THERE ARE MANY MORE TOOLS FOR TESTING JAVASCRIPT.** Go through this link- https://dzone.com/articles/how-to-test-javascript-code-in-a-browser

## WHAT ARE LINTERS IN JAVASCRIPT?
- **JavaScript linters are tools that you can use to help you debug your code. They scan your scripts for common issues and errors, and give you back a report with line numbers that you can use to fix things.**
- In addition to actual bugs and errors, they also check for subjective, stylistic preferences as well.
### Some popular linting tools:
1. **JSLint**
  - Highly opinionated and based on Douglas Crockford’s Javascript: The Good Parts, it does not allow for much configuration.
2. **JSHint**
  -  Comes loaded with sensible defaults, but allows for a lot more configuration than JSLint.
3. **ESLint**
  - An extremely configurable linter that also supports JSX and can autoformat scripts to match your preferred code formatting style.

#### NOTE: 
**They also all offer plugins for text editors like Sublime Text, VS Code, and Atom. These let you lint your code directly in your favorite text editor.**
 
