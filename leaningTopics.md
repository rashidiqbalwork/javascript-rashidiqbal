## OPERATORS:
## 1- Arithmetic operators:-
var x = 5, y = 10;

var z = x + y; //performs addition and returns 15

z = y - x; //performs subtraction and returns 5

z = x * y; //performs multiplication and returns 50

z = y / x; //performs division and returns 2

z = x % 2; //returns division remainder 1

Example: Post and Pre Increment/Decrement
var x = 5;

- **These are the unary operator**

x++; //post-increment, x will be 5 here and 6 in the next line

++x; //pre-increment, x will be 7 here  

x--; //post-decrement, x will be 7 here and 6 in the next line

--x; //pre-decrement, x will be 5 here

 **+Operator with String**
var a = 5, b = "Hello ", c = "World!", d = 10;

a + b; //returns "5Hello "

b + c; //returns "Hello World!"

a + d; //returns 15

b + true; //returns "Hello true"

c - b; //returns NaN; - operator can only used with numbers
### 2- Comparison operator:
.
